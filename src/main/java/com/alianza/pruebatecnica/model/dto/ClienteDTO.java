package com.alianza.pruebatecnica.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO {

    private Long id;
    private String sharedKey;
    private String businessId;
    private String email;
    private Long phone;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataAdded;

}
