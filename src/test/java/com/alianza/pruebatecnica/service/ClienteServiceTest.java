package com.alianza.pruebatecnica.service;

import com.alianza.pruebatecnica.model.dto.ClienteDTO;
import com.alianza.pruebatecnica.model.entitys.Cliente;
import com.alianza.pruebatecnica.repository.ClienteRepository;
import com.alianza.pruebatecnica.service.impl.ClienteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ClienteServiceTest {

    @Mock
    private ClienteRepository clienteRepository;

    @InjectMocks
    private ClienteServiceImpl clienteService;



    @Test
    void listarClientes_CuandoNoHayClientes() {
        // Arrange
        when(clienteRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        List<ClienteDTO> resultado = clienteService.listarCliente();

        // Assert
        assertTrue(resultado.isEmpty());
        verify(clienteRepository, times(1)).findAll();
    }


    @Test
    void buscarClientePorSharedKey_CuandoClienteNoExiste() {
        // Arrange
        String sharedKey = "claveInexistente";
        when(clienteRepository.findBySharedKey(sharedKey)).thenReturn(Optional.empty());

        // Act
        Optional<ClienteDTO> resultado = clienteService.buscarClientePorSharedKey(sharedKey);

        // Assert
        assertTrue(resultado.isEmpty());
        verify(clienteRepository, times(1)).findBySharedKey(sharedKey);
    }




    @Test
    void generarCsvConClientes() {
        // Arrange
        ClienteDTO cliente1 = new ClienteDTO();
        cliente1.setId(1L);
        cliente1.setSharedKey("prueba1");
        cliente1.setBusinessId("clave1");
        cliente1.setEmail("cliente1@ejemplo.com");
        cliente1.setPhone(123456789L);
        cliente1.setDataAdded(LocalDate.of(2024, 4, 9));

        ClienteDTO cliente2 = new ClienteDTO();
        cliente2.setId(2L);
        cliente2.setSharedKey("clave2");
        cliente2.setBusinessId("negocio2");
        cliente2.setEmail("cliente2@ejemplo.com");
        cliente2.setPhone(987654321L);
        cliente2.setDataAdded(LocalDate.of(2024, 4, 10));

        List<ClienteDTO> clientes = Arrays.asList(cliente1, cliente2);

        // Act
        String resultadoCsv = clienteService.generarCsv(clientes);

        // Assert
        String csvEsperado = "Id, Shared Key, Business ID, Email, Phone, Data Added\n"
                + "1,prueba1,clave1,cliente1@ejemplo.com,123456789,09/04/2024\n"
                + "2,clave2,negocio2,cliente2@ejemplo.com,987654321,10/04/2024\n";

        assertEquals(csvEsperado, resultadoCsv);
    }

}
