package com.alianza.pruebatecnica.repository;

import com.alianza.pruebatecnica.model.entitys.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    Optional<Cliente> findBySharedKey(String sharedKey);

    @Query("SELECT c FROM Cliente c WHERE " +
            "(:sharedKey IS NULL OR c.sharedKey = :sharedKey) AND " +
            "(:businessId IS NULL OR c.businessId  = :businessId) AND " +
            "(:phone IS NULL OR c.phone = :phone) AND " +
            "(:email IS NULL OR c.email = :email)")
    List<Cliente> buscarPorCriterios(@Param("sharedKey") String sharedKey,
                                     @Param("businessId") String businessId,
                                     @Param("phone") Integer phone,
                                     @Param("email") String email);

}
