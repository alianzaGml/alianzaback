package com.alianza.pruebatecnica.controller;

import com.alianza.pruebatecnica.model.dto.ClienteDTO;
import com.alianza.pruebatecnica.model.entitys.Cliente;
import com.alianza.pruebatecnica.service.ClienteService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/clientes")
@CrossOrigin(origins = "http://localhost:4200")
public class ClienteController {

    private static final Logger log = LoggerFactory.getLogger(ClienteController.class);

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<ClienteDTO>> listarClientes() {
        log.info("Listando todos los clientes");
        List<ClienteDTO> clientes = clienteService.listarCliente();
        if (clientes.isEmpty()) {
            log.warn("No se encontraron clientes");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<ClienteDTO> clienteDTOs = clientes.stream()
                .map(cliente -> modelMapper.map(cliente, ClienteDTO.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(clienteDTOs, HttpStatus.OK);
    }

    @GetMapping("/{sharedKey}")
    public ResponseEntity<ClienteDTO> buscarClientePorSharedKey(@PathVariable String sharedKey) {
        log.info("Buscando cliente con sharedKey");
        Optional<ClienteDTO> cliente = clienteService.buscarClientePorSharedKey(sharedKey);
        return cliente.map(value -> new ResponseEntity<>(modelMapper.map(value, ClienteDTO.class), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<ClienteDTO> crearCliente(@RequestBody ClienteDTO clienteDTO) {
        try {
            log.info("Creando cliente");
            Cliente cliente = modelMapper.map(clienteDTO, Cliente.class);
            ClienteDTO clienteCreadoDTO = clienteService.crearCliente(cliente);
            return ResponseEntity.status(HttpStatus.CREATED).body(clienteCreadoDTO);

        } catch (Exception e) {
            log.error("Error al crear el cliente");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/buscar")
    public ResponseEntity<List<ClienteDTO>> buscarClienteAvanzado(
            @RequestParam(required = false) String sharedKey,
            @RequestParam(required = false) String businessId,
            @RequestParam(required = false) Integer phone,
            @RequestParam(required = false) String email) {
        log.warn("Buscando con criterios");
        List<ClienteDTO> resultado = clienteService.buscarPorCriterios(sharedKey, businessId, phone, email);
        if (resultado.isEmpty()) {
            log.error("Fallo la busqueda");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<ClienteDTO> resultadoDTOs = resultado.stream()
                .map(cliente -> modelMapper.map(cliente, ClienteDTO.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(resultadoDTOs, HttpStatus.OK);
    }

    @GetMapping("/exportar")
    public ResponseEntity<byte[]> exportarClienteCsv() {
        List<ClienteDTO> clientes = clienteService.listarCliente();
        String csvString = clienteService.generarCsv(clientes);
        byte[] buf = csvString.getBytes();

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=clientes.csv");
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);

        return new ResponseEntity<>(buf, headers, HttpStatus.OK);
    }

}
