package com.alianza.pruebatecnica.service.impl;

import com.alianza.pruebatecnica.model.dto.ClienteDTO;
import com.alianza.pruebatecnica.model.entitys.Cliente;
import com.alianza.pruebatecnica.repository.ClienteRepository;
import com.alianza.pruebatecnica.service.ClienteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ClienteDTO> listarCliente() {
        List<Cliente> clientes = clienteRepository.findAll();
        return clientes.stream()
                .map(cliente -> modelMapper.map(cliente, ClienteDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ClienteDTO> buscarClientePorSharedKey(String sharedKey) {
        Optional<Cliente> cliente = clienteRepository.findBySharedKey(sharedKey);
        return cliente.map(value -> modelMapper.map(value, ClienteDTO.class));
    }

    @Override
    public ClienteDTO crearCliente(Cliente clienteDTO) {
        Cliente cliente = modelMapper.map(clienteDTO, Cliente.class);
        Cliente clienteGuardado = clienteRepository.save(cliente);
        return modelMapper.map(clienteGuardado, ClienteDTO.class);
    }

    @Override
    public List<ClienteDTO> buscarPorCriterios(String sharedKey, String businessId, Integer phone, String email) {
        List<Cliente> clientes = clienteRepository.buscarPorCriterios(sharedKey, businessId, phone, email);
        return clientes.stream()
                .map(cliente -> modelMapper.map(cliente, ClienteDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public String generarCsv(List<ClienteDTO> clientes) {
        StringBuilder csvBuilder = new StringBuilder("Id, Shared Key, Business ID, Email, Phone, Data Added\n");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        for (ClienteDTO cliente : clientes){
            csvBuilder.append(cliente.getId()).append(",");
            csvBuilder.append(cliente.getSharedKey()).append(",");
            csvBuilder.append(cliente.getBusinessId()).append(",");
            csvBuilder.append(cliente.getEmail()).append(",");
            csvBuilder.append(cliente.getPhone()).append(",");
            csvBuilder.append(cliente.getDataAdded().format(formatter)).append("\n");
        }
        return csvBuilder.toString();
    }

}
