package com.alianza.pruebatecnica.controller;


import com.alianza.pruebatecnica.model.dto.ClienteDTO;
import com.alianza.pruebatecnica.model.entitys.Cliente;
import com.alianza.pruebatecnica.service.ClienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
public class ClienteControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ClienteService clienteService;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ClienteController clienteController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(clienteController).build();
    }

    @Test
    public void listarClientes_CuandoHayClientes() throws Exception {
        // Arrange
        List<ClienteDTO> clientesDTO = Arrays.asList(new ClienteDTO(), new ClienteDTO());
        when(clienteService.listarCliente()).thenReturn(clientesDTO);
        when(modelMapper.map(any(ClienteDTO.class), any())).thenReturn(new ClienteDTO());

        // Act & Assert
        mockMvc.perform(get("/api/clientes"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(clientesDTO.size()));
        verify(clienteService).listarCliente();
    }

    @Test
    public void listarClientes_CuandoNoHayClientes() throws Exception {
        // Arrange
        when(clienteService.listarCliente()).thenReturn(Collections.emptyList());

        // Act & Assert
        mockMvc.perform(get("/api/clientes"))
                .andExpect(status().isNoContent());
        verify(clienteService).listarCliente();
    }

    @Test
    public void buscarClientePorSharedKey_CuandoClienteExiste() throws Exception {
        // Arrange
        String sharedKey = "claUnicaPrueba";
        ClienteDTO clienteDTO = new ClienteDTO();
        when(clienteService.buscarClientePorSharedKey(sharedKey)).thenReturn(Optional.of(clienteDTO));
        when(modelMapper.map(any(ClienteDTO.class), any())).thenReturn(new ClienteDTO());

        // Act & Assert
        mockMvc.perform(get("/api/clientes/{sharedKey}", sharedKey))
                .andExpect(status().isOk());
        verify(clienteService).buscarClientePorSharedKey(sharedKey);
    }

    @Test
    public void buscarClientePorSharedKey_CuandoClienteNoExiste() throws Exception {
        // Arrange
        String sharedKey = "claveInexistente";
        when(clienteService.buscarClientePorSharedKey(sharedKey)).thenReturn(Optional.empty());

        // Act & Assert
        mockMvc.perform(get("/api/clientes/{sharedKey}", sharedKey))
                .andExpect(status().isNotFound());
        verify(clienteService).buscarClientePorSharedKey(sharedKey);
    }

    @Test
    public void exportarClienteCsv_CuandoHayClientes() throws Exception {
        // Arrange
        ClienteDTO cliente1DTO = new ClienteDTO();
        ClienteDTO cliente2DTO = new ClienteDTO();
        List<ClienteDTO> clientesDTO = Arrays.asList(cliente1DTO, cliente2DTO);
        String csvEsperado = "contenido,csv,esperado";

        when(clienteService.listarCliente()).thenReturn(clientesDTO);
        when(clienteService.generarCsv(clientesDTO)).thenReturn(csvEsperado);

        // Act & Assert
        mockMvc.perform(get("/api/clientes/exportar"))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=clientes.csv"))
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE))
                .andExpect(content().bytes(csvEsperado.getBytes()));

        verify(clienteService).listarCliente();
        verify(clienteService).generarCsv(clientesDTO);
    }

}
