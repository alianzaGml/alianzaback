package com.alianza.pruebatecnica.service;

import com.alianza.pruebatecnica.model.dto.ClienteDTO;
import com.alianza.pruebatecnica.model.entitys.Cliente;

import java.util.List;
import java.util.Optional;

public interface ClienteService {

    List<ClienteDTO> listarCliente();
    Optional<ClienteDTO> buscarClientePorSharedKey(String sharedKey);
    //Cliente crearCliente(Cliente cliente);

    ClienteDTO crearCliente(Cliente cliente);

    List<ClienteDTO> buscarPorCriterios(String sharedKey, String businessId, Integer phone, String email);
    String generarCsv(List<ClienteDTO> clientes);

}
